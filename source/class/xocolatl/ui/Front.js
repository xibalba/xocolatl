//

qx.Class.define("xocolatl.ui.Front", {
	extend: qx.ui.core.Widget,
	type: "singleton",

	construct: function() {
		this.base(arguments);

		this._setLayout(new qx.ui.layout.HBox());

		// this._add(this.getToolBar());
		this._add(this.getTabView(), { flex: 1 });
	},

	members: {
		__tabView: null,

		getTabView: function() {
			if(this.__tabView) return this.__tabView;

			this.__tabView = new qx.ui.tabview.TabView();
			this.__tabView.add(xocolatl.ui.tabpages.Dashboard.getInstance());

			return this.__tabView;
		},

		// Decorador
		/*getToolBar: function() {
			return mapache.ui.toolbar.Front.getInstance();
		}*/
	}
});
