//

qx.Class.define("xocolatl.ui.tabpages.Dashboard", {
	extend: qx.ui.tabview.Page,
	type: "singleton",

	construct: function() {
		// , "mapache/icons/actions/24/dashboard-show.svg"
		this.base(arguments, "Tablero de Cuentas");
		// this.getChildControl("button").getChildControl("icon").set({width: 24, height: 24});

		this.setLayout(new qx.ui.layout.Grow());
		this._add(this.getTree());
	},

	members: {
		__accountsTree: null,

		getTree: function() {
			if(this.__accountsTree) return this.__accountsTree;

			this.__accountsTree = new qx.ui.tree.Tree();

			let root = new qx.ui.tree.TreeFolder("Cuentas");

			this.__accountsTree.setRoot(root);

			root.setOpen(true);

			this.__accountsTree.setRootOpenClose(true);

			return this.__accountsTree;
		},

		composeLeaf: function(data) {
			this.getTree().getRoot().add(new qx.ui.tree.TreeFolder(data["etiqueta"]));
		}
	}
});
