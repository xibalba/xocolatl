/* ************************************************************************

   Copyright: 2020 undefined

   License: MIT license

   Authors: undefined

************************************************************************ */

/**
 * This is the main application class of "xocolatl"
 *
 * @asset(xocolatl/*)
 */
qx.Class.define("xocolatl.Application", {
	extend : qx.application.Standalone,

	/*
	*****************************************************************************
	 MEMBERS
	*****************************************************************************
	*/
	members: {
		/**
		 * This method contains the initial application code and gets called
		 * during startup of the application
		 *
		 * @lint ignoreDeprecated(alert)
		 */
		 main: function() {
			 this.base(arguments);

			// Enable logging in debug variant
			if(qx.core.Environment.get("qx.debug")) {
				// support native logging capabilities, e.g. Firebug for Firefox
				qx.log.appender.Native;
			}

			this.getRoot().add(xocolatl.controller.Front.getInstance().getView(), { edge: 0 });
		}
	}
});
