/* ************************************************************************

   Copyright: 2020 undefined

   License: MIT license

   Authors: undefined

************************************************************************ */

qx.Theme.define("xocolatl.theme.Theme",
{
  meta :
  {
    color : xocolatl.theme.Color,
    decoration : xocolatl.theme.Decoration,
    font : xocolatl.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : xocolatl.theme.Appearance
  }
});